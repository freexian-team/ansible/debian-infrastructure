# Debusine-server role

The role takes care of almost everything for you. The remaining steps that
you have to handle are:

* configuring the HTTPS certificate management and the corresponding
  configuration entries in the virtual host (if you use HTTPS). The playbook
  can set Nginx for you if the variable `debusine_server_setup_nginx` is true.
  You will need to modify `/etc/nginx/sites-available/debusine.conf` to
  enable HTTPS for debusine-server
* Approve workers when they connect to the instance
 (see https://freexian-team.pages.debian.net/debusine/admin/first-steps.html#enable-a-new-worker-on-the-server-based-on-the-worker-s-name)
* Create users and tokens for the debusine clients
 (see https://freexian-team.pages.debian.net/debusine/admin/first-steps.html#creating-token-for-debusine-client)

# Role variables

## Variables

* `debusine_server_fqdns`: a list of server fully qualified domain names used
  by the service (set the application and reverse proxy virtual hosts). The
  first element is the primary server name and used for sending mail.
* `debusine_server_setup_nginx`: true/false. If true: set up nginx
* `debusine_use_snapshot_repository`: if true use the latest debusine devel build.
  Otherwise, use the debusine available on deb.freexian.com
* `debusine_mailserver`: not set, none or 'postfix'. If 'postfix' install postfix package. It does
  not set up Postfix. Debusine will try to use localhost to send notification emails
* `debusine_server_configuration`: Optional debusine server configuration in
  Python syntax to be included in the `local.py`.

To manually set up nginx, set the variable `debusine_server_setup_nginx` to
`false`. In your nginx debusine server configuration, include the line:
```
include /etc/nginx/snippets/debusine.conf;
```

This configuration file is provided with the `debusine-server` package.

# Example playbook
```
---
- name: Install debusine-server
  hosts: debusine_server
  vars:
    debusine_server_fqdns:
     - debusine.server.com
    debusine_server_setup_nginx: true
    debusine_use_snapshot_repository: true
  tasks:
    - name: Install debusine-server
      ansible.builtin.include_role:
        name: freexian.debian_infrastructure.debusine_server
```

# Creating a workflow template

The `workflow_template` task can be used to create a workflow template. It
consumes the following variables:

* `debusine_workflow_template_name`: the name of the workflow (required)
* `debusine_workflow_template_task`: the workflow to use e.g. `noop`, `sbuild` or `update_environments` (required)
* `debusine_workflow_template_workspace`: the name of the workspace to use (defaults to `System`)
* `debusine_workflow_template_data`: the `task_data` object, not string (required)

For more information on these variables refer to
https://freexian-team.pages.debian.net/debusine/reference/workflows.html.

# License

MIT

# Author Information

Carles Pina i Estany <carles@pina.cat>

