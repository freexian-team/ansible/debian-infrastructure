# Mirror-status role

The role takes care of almost everything for you. The remaining steps that
you have to handle are:

* configuring the HTTPS certificate management and the corresponding
  configuration entries in the virtual host (if you use https)
* enabling the virtual host (either in nginx or in apache2)

# Role variables

## Mandatory variables

* `mirror_status_fqdn`: the fully qualified domain name used by the
  service. This value is used as server name for the website.
* `mirror_status_git_url`: the URL of the mirror-status git repo.
* `mirror_status_git_update_script`: the path to the script that cron runs
  periodically to update mirror-status, relative to the mirror-status git
  checkout.

## Optional variables

* `mirror_status_homedir`: the home directory of the `mirror-status` user.
* `mirror_status_webserver`: select which webserver to configure for,
  between "nginx" and "apache2". Disabled by default, meaning that the role
  doesn't try to configure the webserver.
* `mirror_status_website_name`: the site configuration filename, without
  extension. Useful only if `mirror_status_webserver` is set.

# Example playbook

```
- name: Install mirror-status
  ansible.builtin.include_role:
    name: freexian.debian_infrastructure.mirror_status
    apply:
      tags:
        - mirror-status
  vars:
    mirror_status_fqdn: mirror-status.kali.org
    mirror_status_git_url: https://gitlab.com/kalilinux/tools/mirror-status
    mirror_status_git_update_script: update-kali

- name: Install extra packages for the update script
  ansible.builtin.apt:
  update_cache: true
  name:
    - rsync
```

# License

MIT

# Author Information

Raphaël Hertzog <raphael@freexian.com>
Arnaud Rébillout <arnaudr@kali.org>
