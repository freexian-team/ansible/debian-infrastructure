debci\_common
=============

Base role for installing debci components. It establishes the right apt sources
and pinning and installs the `debci` package.

Role Variables
--------------

`debci_amqp_server` identifies the AMQP server used by all `debci` components.
When unset, `debci` defaults to using the loopback server in which case you
should also run the `rabbitmq` role. It should be formatted as
`amqp://hostname` or `amqps://hostname`. In the latter case, also set
`debci_certificates`.

`debci_certificates` is a map of bare hostnames. The special name `ca`
identifies a private certificate authority. `debci_certificates.ca.crt` should
contain the certificate in PEM format. Looking up the `ansible_hostname` should
result in a map that maps `crt` to the host certificate and `key` to the host
key. `debci` will then use this key and certificate as a client certificate for
connecting to a remote `rabbitmq` instance. `debci_certificates` should not be
set unless setting `debci_amqp_server` to an `amqps://...` server.

License
-------

MIT

Author Information
------------------
Sébastien Delafond <sdelafond@gmail.com>
Helmut Grohne <helmut@freexian.com>
