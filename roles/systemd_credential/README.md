systemd\_credential
===================

Store a credential in the systemd credential store. After storing a credential
as a given name, the name may be used in systemd units with `LoadCredential`.

Role Variables
--------------

 * `name` (required): Name of the credential.
 * `credential` (required): Secret value to be stored.
 * `systemd_credential_storelocation` (optional): Location of the credential
   store, defaults to `/etc/credstore`.

License
-------

MIT

Author Information
------------------
Helmut Grohne <helmut@freexian.com>
