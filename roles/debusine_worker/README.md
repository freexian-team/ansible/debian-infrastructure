# Debusine-worker role

The role set up a debusine worker. The worker is first configured to connect to
the specified debusine server, and then the daemon is started which will trigger
the outgoing connection. Before being able to process work requests, the worker
still has to be approved on the server side.

The worker is also configured to meet the requirements of being able to
execute specific tasks, such as building Debian packages or running
autopkgtests.

# Role variables

## Variables

* `debusine_server_api_url`: Debusine server API URL. E.g. `https://debusine.distro.org/api`
* `debusine_use_snapshot_repository`: if true use the latest debusine devel build.
  Otherwise, use the debusine available on deb.freexian.com
* `debusine_worker_enable_tasks`: list the tasks that the worker must be
  able to run. Any required setup will be made.
* `debusine_task_sbuild_chroots`: the list of sbuild chroots to create.
  Each chroot is described with a dictionary with two keys: `distribution`
  and `architecture`.

# Example playbook
```
---
- name: Install debusine-worker
  hosts: debusine_worker
  vars:
    debusine_server_api_url: http://localhost/api
    debusine_use_snapshot_repository: true
    debusine_worker_enable_tasks:
    - sbuild
    - piuparts
    - mmdebstrap
    - autopkgtest
    - lintian
    - blhc
    debusine_worker_enable_backends:
    - incus-lxc
    - incus-vm
    - schroot
    - qemu
    - unshare
    debusine_task_sbuild_chroots:
    - distribution: 'debian:bookworm'
      architecture: amd64
    - distribution: 'debian:unstable'
      architecture: amd64
  tasks:
    - name: Install debusine-worker
      ansible.builtin.include_role:
        name: freexian.debian_infrastructure.debusine_worker
```

# License

MIT

# Author Information

Carles Pina i Estany <carles@pina.cat>

