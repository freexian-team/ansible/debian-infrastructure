# Debusine-signing worker role

The role set up a debusine signing worker. A signing database is initialized on
the same host and it connects to the specified debusine server.  Before being
able to process work requests, the worker still has to be approved on the
server side.

No HSM integration is configured, but a secret is created to encrypt the keys
stored in the database. Refer to [the
documentation](https://freexian-team.pages.debian.net/debusine/howtos/set-up-debusine-signing.html#notes-for-production-deployments)
for more information.

# Role variables

## Variables

* `debusine_server_api_url` (mandatory): Debusine server API URL. E.g.
  `https://debusine.distro.org/api`
* `debusine_signing_yubihsm`: Set to true if you have a YubiHSM.

# Example playbook
```
---
- name: Install debusine-signing
  hosts: debusine_signers
  vars:
    debusine_server_api_url: http://localhost/api
  tasks:
    - name: Install debusine-signing
      ansible.builtin.include_role:
        name: freexian.debian_infrastructure.debusine_signing
```

# License

MIT

# Author Information

Helmut Grohne <helmut@freexian.com>

