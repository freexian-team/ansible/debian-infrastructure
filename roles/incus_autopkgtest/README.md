# incus role

Install the incus container/vm manager and configure defaults for use as an
autopkgtest backend.

# Role variables

* `incus_autopkgtest_preseed`: A JSON object representing the [incus preseed
   configuration](https://linuxcontainers.org/incus/docs/main/howto/initialize/#non-interactive-configuration).
   It will be encoded to YAML as expected by `incus admin init --preseed`.
