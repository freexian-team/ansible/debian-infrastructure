debian\_backports
=================

Enable the `-backports` repository for the current Debian release as an apt
source.

Role Variables
--------------

 * `debian_backports_mirror`: Can be changed to use a custom Debian mirror.
 * `debian_backports_suite`: Can be used to choose a custom suite such as
   `bullseye-backports-sloppy`.
 * `debian_backports_components`: Is a list of enabled components and defaults
   to `main` only.
 * `debian_backports_filename`: Is the file name without extension in
   `/etc/apt/sources.list.d` and defaults to the the `debian_backports_suite`.

License
-------

MIT

Author Information
------------------

Helmut Grohne <helmut@freexian.com>
