servertasks
===========

The servertasks role actually is the entire software and installation procedure.
It is a mini-framework for running user-defined tasks at regular intervals in
Debian-containers with access to pre-defined credentials.

The state lives in `/var/local/servertasks` and it is configured via
`/etc/servertasks/servertasks.conf`. The format of this file is based on
Python's argparse options with double-dashes prepended. It should be managed
with `ansible.builtin.lineinfile` to allow credentials by adding a line
`allow-credential={{ groupname }}:{{ credentialname }}` after relevant
credentials have been added to systemd's system credential store by unspecified
means. The simplest way is dropping them below `/etc/credstore`.


Internals
---------

The `--base-dir` (default: `/var/local/servertasks`) directory is considered to
be internal territory and poking at things may have undesired consequences. It
has the following entries.
 * The `code` directory contains a copy of the source that runs a task group.
   The copy is made when installing the group and it will be named
   `$GROUPNAME-$TIMESTAMP` together with a symbolic link `$GROUPNAME` pointing
   at the current version. Keeping multiple copies ensures that a running task
   will not have its software changed while running. The current version of
   such a directory will be made available read-only to a task via the
   environment variable `$CODE_DIRECTORY`.
 * The `chroots` directory contains chroots stored by their configuration hash
   `$CONFIGHASH-$TIMESTAMP` together with a symbolic link `$CONFIGHASH`
   pointing at the current version. The hash captures those aspects of a
   `servertasks.yaml` that affect the chroot. Hence, reinstalling a task group
   only requires recreating a chroot if relevant parameters are changed and
   multiple task groups can share a chroot if they happen to use the same
   chroot description. Keeping multiple copies ensures that a running task will
   not have its chroot changed while running.
 * The `data` directory contains subdirectories `$GROUPNAME/$TASKNAME` for each
   task in the group. This folder will be accessible to a task via the
   environment variable `$DATA_DIRECTORY`.
 * The `groupdata` directory contains a directory `$GROUPNAME` that will be
   accessible to all tasks of the group via the via the environment variable
   `$GROUPDATA_DIRECTORY`.
 * The `triggers` directory contains empty files `$GROUPNAME/$TASKNAME` for
   each task in a group and the group directory is made available to all tasks
   of a group via the `$TRIGGER_DIRECTORY` environment variable. Touching a
   task's file causes the task to be run. Hence multiple tasks can be chained
   together and tasks can be triggered externally.

To facilitate cleanup of `code` and `chroots`, tasks runtime is always limited
(`--task-timeout` defaults to one day) and old instances are automatically
deleted when they are known to be unused assuming that they don't exceed their
timeout.

License
-------

MIT

Author Information
------------------
Helmut Grohne <helmut@freexian.com>
