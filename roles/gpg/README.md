Gpg
===

Create or import GPG keys in a user account.

Role Variables
--------------

Common variables:
* `gpg_user`: the user account where the work will be performed

Variables to create a GPG key with gpg --batch:
* `gpg_key_email`: email associated to the new GPG key
* `gpg_key_realname`: name associated to the new GPG key
* `gpg_key_type`: type of GPG key created (defaults to "RSA")
* `gpg_key_length`: size in bits of the new GPG key (defaults to 2048)
* `gpg_key_usage`: expected usage of the key (defaults to "sign")
* `gpg_expire_date`: defines when the key will expire (defaults to "0",
  meaning no expiration)

Variables to import GPG keys:
* `gpg_keys`: list of keys' identifiers to import
* `gpg_keyserver`: name of the keyserver to use to download the keys

Example Playbook
----------------

To create a new key:
```
- name: Create GPG key
  ansible.builtin.include_role:
    name: freexian.debian_infrastructure.gpg
    tasks_from: genkey
  vars:
    gpg_user: deblts
    gpg_key_realname: 'Freexian Admins'
    gpg_key_email: 'sysadmin@freexian.com'
    gpg_key_type: RSA
    gpg_key_length: 4096
    gpg_key_usage: sign
    gpg_expire_date: '5y'

```

To import keys:
```
- name: Import keys of uploaders
  include_role:
    name: freexian.debian_infrastructure.gpg
    tasks_from: import
  vars:
    gpg_user: deblts
    gpg_keys: 
      - C74F6AC9E933B3067F52F33FA459EC6715B0705F
      - F2FAAC0D44C32D8B98539B9297A0FA0FC8F2DE45
    gpg_keyserver: keyring.debian.org

```

License
-------

MIT

Author Information
------------------

Raphaël Hertzog <raphael@freexian.com>
